//
//  APIManager.m
//  MapPark
//
//  Created by Francesco Mattia on 18/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import "APIManager.h"
@implementation APIManager

NSString *const kBaseURL = @"https://api.parkatmyhouse.com/1.1/";
//NSString *const kBaseURL = @"http://127.0.0.1:3000/";

+ (void)serchParksAtLocation:(NSString*)location withCompletion:(void (^)(NSDictionary*, NSError*))block
{
    NSString *path = [NSString stringWithFormat:@"location/?q=%@", location];
    NSString *url = [kBaseURL stringByAppendingString:path];
    url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        block(responseObject,nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"API Failed: %d %@",[operation.response statusCode], [error localizedDescription]);
        block(nil,error);
    }];
    
    [operation start];
}

@end