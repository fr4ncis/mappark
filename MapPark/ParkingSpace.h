//
//  ParkingSpace.h
//  MapPark
//
//  Created by Francesco Mattia on 19/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import <Foundation/Foundation.h>

@import MapKit;

@interface ParkingSpace : NSObject  <MKAnnotation>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSString *category;

+ (instancetype)instanceFromJSON:(NSDictionary*)dictionary;

@end
