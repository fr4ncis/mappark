//
//  ListingsView.m
//  MapPark
//
//  Created by Francesco Mattia on 19/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import "ParkingSpaceView.h"
#import <ViewUtils.h>

@implementation ParkingSpaceView {
    float originalBottom, originalHeight;
}
- (id)init
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        UIView *view = [nib objectAtIndex:0];
        view.layer.cornerRadius = 8.0f;
        view.layer.shadowRadius = 3.0f;
        view.layer.shadowColor = [UIColor blackColor].CGColor;
        view.layer.shadowOffset = CGSizeMake(0, 0);
        view.layer.shadowOpacity = 0.8f;
        self = (ParkingSpaceView*)view;
        [self setUp];
    }
    return self;
}

- (void)setUp
{
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    pan.delegate = self;
    self.height = 200.0f;
    originalBottom = self.bottom;
    originalHeight = self.height;
    [self addGestureRecognizer:pan];
}

- (void)handlePan:(UIPanGestureRecognizer*)gesture
{
    const float threshold = 0.6f;
    CGPoint point = [gesture translationInView:[self superview]];
    float percentage = MIN(1,MAX(0, fabsf(point.y) / 100.0));
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        [UIView animateWithDuration:0.2 animations:^{
            if (percentage > threshold)
            {
                self.height = 400;
            }
            else
            {
                self.height = originalHeight;
            }
        }];
    }
    else
    {
        self.height = MAX(400*percentage,originalHeight);
    }
    self.bottom = originalBottom;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
