//
//  FMViewController.h
//  MapPark
//
//  Created by Francesco Mattia on 18/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iCarousel.h>

@interface FMViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>

@end
