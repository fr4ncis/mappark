//
//  main.m
//  MapPark
//
//  Created by Francesco Mattia on 18/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FMAppDelegate class]));
    }
}
