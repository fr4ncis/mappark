//
//  APIManager.h
//  MapPark
//
//  Created by Francesco Mattia on 18/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@interface APIManager : AFHTTPRequestOperationManager

+ (void)serchParksAtLocation:(NSString*)location withCompletion:(void (^)(NSDictionary*, NSError*))block;

@end
