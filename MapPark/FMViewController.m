//
//  FMViewController.m
//  MapPark
//
//  Created by Francesco Mattia on 18/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import "FMViewController.h"
#import "APIManager.h"
#import <ViewUtils.h>
#import "ParkingSpace.h"
#import "ParkingSpaceView.h"
#import "AnnotationView.h"
#import "TargetAnnotation.h"

@import MapKit;

@interface FMViewController () {
    NSMutableArray *results;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;

@end

@implementation FMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    results = [NSMutableArray new];
    
    self.carousel.top = self.view.height;
    self.carousel.type = iCarouselTypeRotary;
    
    NSString *searchString = @"Wembley";
    [APIManager serchParksAtLocation:searchString withCompletion:^(NSDictionary *dict, NSError *error) {
        if (!error)
        {
            [self.navigationItem setTitle:searchString];
            if ([self handleResponse:dict]) return;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error occurred" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)handleResponse:(NSDictionary*)dict
{
    NSLog(@"Dictionary %@", dict[@"coords"]);
    
    @try {
        [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake([dict[@"coords"][@"lat"] floatValue], [dict[@"coords"][@"lng"] floatValue])];
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.mapView.centerCoordinate, 300, 300)];
        
        TargetAnnotation *target = [[TargetAnnotation alloc] init];
        target.coordinate = self.mapView.centerCoordinate;
        [self.mapView addAnnotation:target];
        
        NSArray *resultsArray = dict[@"data"];
        
        for (NSDictionary *dict in resultsArray)
        {
            ParkingSpace *ps = [ParkingSpace instanceFromJSON:dict];
            [results addObject:ps];
            [self.mapView addAnnotation:ps];
            NSLog(@"T: %@", ps.title);
        }

    }
    @catch (NSException *exception) {
        return NO;
    }

    [self displayCarousel];
    return YES;
}

- (void)displayCarousel
{
    [self.carousel reloadData];
    [UIView animateWithDuration:0.3f animations:^{
        self.carousel.top = self.view.height-self.carousel.height;
    }];
}

#pragma mark -
#pragma mark iCarousel

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [results count];
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    return 280.0f;
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.15f;
        }
        default:
        {
            return value;
        }
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    ParkingSpaceView *psv = [[ParkingSpaceView alloc] init];
    ParkingSpace *space = results[index];
    psv.titleLabel.text = space.title;
    psv.subtitleLabel.text = space.category;
    return psv;
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(panMapToPoint) object:nil];
    [self performSelector:@selector(panMapToPoint) withObject:nil afterDelay:1.0f];
}

- (void)panMapToPoint
{
    NSLog(@"%d",self.carousel.currentItemIndex);
    ParkingSpace *space = results[[self.carousel currentItemIndex]];
    [self.mapView setCenterCoordinate:space.coordinate animated:YES];
}

#pragma mark -
#pragma mark Map

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    AnnotationView *annotationView = (AnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"mapAnnotation"];
    if (!annotationView)
    {
        annotationView = [[AnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"mapAnnotation"];
    }
    return annotationView;
}


@end
