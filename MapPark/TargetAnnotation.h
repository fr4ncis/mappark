//
//  Annotation.h
//  MapPark
//
//  Created by Francesco Mattia on 20/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import <Foundation/Foundation.h>

@import MapKit;

@interface TargetAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@end
