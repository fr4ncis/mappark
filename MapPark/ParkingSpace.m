//
//  ParkingSpace.m
//  MapPark
//
//  Created by Francesco Mattia on 19/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import "ParkingSpace.h"


@implementation ParkingSpace

+ (instancetype)instanceFromJSON:(NSDictionary*)dictionary
{
    @try {
        ParkingSpace *ps = [ParkingSpace new];
        ps.title = dictionary[@"title"];
        ps.category = dictionary[@"category"];
        ps.coordinate = CLLocationCoordinate2DMake([dictionary[@"coords"][@"lat"] floatValue], [dictionary[@"coords"][@"lng"] floatValue]);
        return ps;
    }
    @finally {
        return nil;
    }

}

@end
