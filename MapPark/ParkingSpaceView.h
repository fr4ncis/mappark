//
//  ListingsView.h
//  MapPark
//
//  Created by Francesco Mattia on 19/05/14.
//  Copyright (c) 2014 Fr4ncis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingSpaceView : UIView <UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabel;

@end
