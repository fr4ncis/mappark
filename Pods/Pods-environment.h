
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 2
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 4

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 4

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 4

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 4

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 4

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 4

// Kiwi
#define COCOAPODS_POD_AVAILABLE_Kiwi
#define COCOAPODS_VERSION_MAJOR_Kiwi 2
#define COCOAPODS_VERSION_MINOR_Kiwi 2
#define COCOAPODS_VERSION_PATCH_Kiwi 4

// Kiwi/ARC
#define COCOAPODS_POD_AVAILABLE_Kiwi_ARC
#define COCOAPODS_VERSION_MAJOR_Kiwi_ARC 2
#define COCOAPODS_VERSION_MINOR_Kiwi_ARC 2
#define COCOAPODS_VERSION_PATCH_Kiwi_ARC 4

// Kiwi/NonARC
#define COCOAPODS_POD_AVAILABLE_Kiwi_NonARC
#define COCOAPODS_VERSION_MAJOR_Kiwi_NonARC 2
#define COCOAPODS_VERSION_MINOR_Kiwi_NonARC 2
#define COCOAPODS_VERSION_PATCH_Kiwi_NonARC 4

// Kiwi/SenTestingKit
#define COCOAPODS_POD_AVAILABLE_Kiwi_SenTestingKit
#define COCOAPODS_VERSION_MAJOR_Kiwi_SenTestingKit 2
#define COCOAPODS_VERSION_MINOR_Kiwi_SenTestingKit 2
#define COCOAPODS_VERSION_PATCH_Kiwi_SenTestingKit 4

// ViewUtils
#define COCOAPODS_POD_AVAILABLE_ViewUtils
#define COCOAPODS_VERSION_MAJOR_ViewUtils 1
#define COCOAPODS_VERSION_MINOR_ViewUtils 1
#define COCOAPODS_VERSION_PATCH_ViewUtils 2

// iCarousel
#define COCOAPODS_POD_AVAILABLE_iCarousel
#define COCOAPODS_VERSION_MAJOR_iCarousel 1
#define COCOAPODS_VERSION_MINOR_iCarousel 7
#define COCOAPODS_VERSION_PATCH_iCarousel 6

